from io import SEEK_END
import yaml
import pyfastg
import itertools
from collections import Counter, defaultdict
import logging
logging.basicConfig(filename='debuglog.log',level=logging.DEBUG)


class Node:
    def __init__(self, name):
        self.name = name
class GraphConnection:
    def __init__(self, graphfile, pathsfile, annotation=None, assembly = "", taxonomy = ""):
        self.graphfile = graphfile
        self.pathsfile = pathsfile
        self.annotation = annotation
        self.assembly = assembly
        self.taxonomy = taxonomy
        self.constructGraphConnection(self.graphfile, self.pathsfile, self.annotation, self.assembly, self.taxonomy)

    def constructGraphConnection(self, graphfile, pathsfile, annotation, taxonomy, assembly):
        if self.annotation != None:
            self.anns = self.yamlconstruction(self.annotation)
            
        self.paths = self.readPaths(self.pathsfile)
        self.graph, self.edges = self.readGraph(self.graphfile)
        self.sequences = self.graphSequences(self.graphfile)
        self.rpaths = defaultdict(list)
        for key, value in self.paths.items():
            for i in value:
                i = i[:-1]
                self.rpaths[i].append(key)

    def yamlconstruction(self, file):
        outstring = ""
        start = True
        entries  = []
        yamldict = {}
        file = open(file,'r').readlines()
        for line in file:
            if line == "contig: \n" and not start:
                entry = outstring

                outstring = line
                entries.append(yaml.full_load(entry))
            else:
                #print(line)
                outstring += line
                start = False
        for entry in entries:
            contigid = entry['contig']['name']

            yamldict[contigid] = entry
        return(yamldict)

    def readPaths(self, file):
        """
        Reads paths file, returns dictionary with assembled sequence and corr. edges
        paths file:
        >Contig
        edge[+-],edge[+-]
        >Contig'
        edge[+-],edge[+-]
        """
        paths = open(file,'r').readlines()
        header = "" 
        pathdict = {}
        for line in paths:
            if line.find("NODE") != -1:
                header = line.strip("\n")
                pathdict[header] = []
            else:
                nodes = line.strip("\n").split(",")
                pathdict[header] = nodes
                #print(header, nodes)
        return(pathdict)

    def graphSequences(self,file):
        """
        Takes the graph file to parse the sequences
        """
        sequences  = {}
        name = ""
        seq = ""
        with open(file,'r') as f:
            lines = f.read().splitlines()
        for line in lines:
            if line[0] == ">" and name != "":
                sequences[name] = [line,seq]
                name = line.strip(">").split(":")[0]
                char = {False:"+",True:"-"}
                orient =  char["'" in name]
                name = name.split("_")[1]
                name += orient
                seq = ""
            elif name == "":
                name = line.strip(">").split(":")[0]
                name = name.split("_")[1]
            else:
                seq += line.strip()
        return(sequences)

    def readGraph(self,file):
        """
        Reads graph file (fastg) and parses contents into links and nodes
        """
        with open(file,'r') as f:
            lines = f.read().splitlines()
        # Only select fastg headers, split on edge char :
        headers = [x for x in lines if ">" in x]
        edged_nodes = [x.split(":") for x in headers]
        graphdict = {}#defaultdict(list)
        edgedict = {}#defaultdict(list)
        lens = [len(x) for x in edged_nodes]
        

        for couple in edged_nodes:
            # Check if nodes have edges
            if len(couple) > 1: 
                # Split into nodes
                node = [couple[0].strip(">")]
                node += couple[1].split(",")
                
                ids = [x.split("_")[1] for x in node] # Edge id's in paths are only the NODE number
                orients = ["-" if "'" in x else "+" for x in node]
                ids = list(map(str.__add__,ids,orients))
                # Retrying but add more information
                graphdict[ids[0]] = [node[0],ids[1:],node[1:]]
                edgedict[ids[0]] = [ids[1:]]
                #for i in ids:
                #    edgedict[ids[i]] = 

            else:
                node = couple[0].strip(">")
                ids = node.split("_")[1]
                char = {False:"+",True:"-"}
                ids += char["'" in ids]
                #else: 
                graphdict[ids] = [node]
                edgedict[ids] = None

        return(graphdict, edgedict)

    def combineFiles(self, graphs, annotations, paths):
        self.contigs = []

    def nodeLookup(self, nodeid):
        
        return(self.graph[nodeid])

    def contigLookup(self, contigid, type="all"):
        """
        Takes contig ID and looks it up in annotations file.
        Returns yaml entry for input contig
        """
        output = []
        try:
            if type == "all" or type =="annotation":
                annotation = self.anns[contigid]
                output += annotation
            if type == "all" or type == "path":
                path = self.paths[contigid]
                output += path
            
            return(output)
        except KeyError:
            return(None)
    
    def nodeLookup_old(self, nodeid):
        """
        Takes node name, looks it up in paths dict
        Returns contigs corresponding to node
        """
        try:
            return(self.rpaths[nodeid])
        except KeyError:
            return(None)

    def outputFastg(nodes, filename):
        pass

    def lookupMGEs(self, mge):
        sane_input = ['plasmid','is','phage']
        if mge.lower() not in sane_input:
            print(f"{mge} is not a valid MGE at this time. Please try plasmid, IS or phage")
            return(None)
        print(self.anns.keys())
        mge_contigs =  []
        for key in self.anns.keys():
            print(key, self.anns[key]['contig'])
            if mge in self.anns[key]['contig']:
                #print(self.anns[key])
                mge_contigs.append(key)
        #print(mge_contigs)
        return(mge_contigs)

    #def writeToGraph(self,fastg, )
    def expandSubgraph(self, es_edges, neighbors=1,neighborlist=[]):
        logging.debug("Here in subgraph")
        nbrdict = defaultdict(list)
        #nbrlist = neighborlist
        swapdict = {'+':'-','-':'+'}
        totallist = []
        # Makes sure one-edge paths can still be parsed
        if type(es_edges) != list:
            es_edges = [es_edges]
        # If a graph is already expanded, it returns a nested list. 
        # To expand these, they need to be flat lists.
        elif type(es_edges[0]) == list:
            es_edges = [i for j in es_edges for i in j]
        # Also add the complemented site
        es_edges += [x[:-1]+swapdict[x[-1]] for x in es_edges]
        es_edges = list(set(es_edges))

        while neighbors > -1:
            for edge in es_edges:
                #totallist.append(edge)
                #print(totallist)
                # Check if edge is in the edges file
                if edge in self.edges.keys():
                    neighbor = self.edges[edge]
                    
                    #other_end = edge[:-1]+swapdict[edge[-1]]
                    if neighbor != None:
                        neighbor = neighbor[0]
                        # If the number of requested neigbhors is 0, only neighbors
                        # that are in the original set are returned
                        if neighbors == 0:
                            neighbor = [x for x in neighbor if x in es_edges]   
                    else:
                        neighbor = ["None"]
                    
                    nbrdict[edge] = neighbor
                    #nbrlist += list(neighbor)
                    totallist += list(neighbor)
                else: 
                    pass # for some reason this causes an infinite loop if just pass

            neighbors -= 1
            #print(neighbors)
            es_edges = list(set(totallist))
        return(nbrdict)
        # TODO: Recursive for higher degree neighbors 
        #neighbors -=1
        #print(f"{neighbors} with {totallist}")
        #nbrlist = list(set(nbrlist))
        #if neighbors > 0:
        #    nbrlist = self.expandSubgraph(totallist, neighbors, neighborlist=totallist)
        #print(f'Done with iteration {neighbors}. Going up!')
        #logging.debug(f"NEIGHBOURS\n{nbrlist}\n")
        #if neighbors == 0:
        #    print("Done!",neighbors)
        #    return nbrdict#.append(neighborlist)
        #else:
        #    print(f"returning {totallist}\n{nbrdict}")
        #    return list(set(totallist))
        
    def reconstructOneFastg(self,keynode, nbrs):
        """
        Reconstructs the fastg sequence of one entry.
        """
        
        keynode_info = self.nodeLookup(keynode)
        node_header = keynode_info[0]
        newheader  = f">{node_header}:"
        node_sequence = self.sequences[keynode][1]
        if nbrs[0] != "None":
           
            
            existing_edges = keynode_info[1]
            #print(node_header,existing_edges)
            realized_edges = list(set(nbrs) & set(existing_edges))
            logging.debug(f"for {keynode}: {existing_edges} || {nbrs} = {realized_edges}")


            nbr_info = [self.nodeLookup(x) for x in realized_edges]
            nbr_headers = [x[0] for x in nbr_info]

            for i in range(len(nbr_headers)):
                if i > 0:
                    newheader += ","
                newheader += nbr_headers[i]
            newheader+=";"
            
        return(f"{newheader}\n{node_sequence}\n")
        


    def writeAnnotation(self, paths, name, file, mode='w'):
        lines = ""
        with open(file,mode) as annotfile:
            logging.debug(f"{paths[:-1]}, {name}")
            for node in paths[0]:
                lines+= f"{node[:-1]},{name}\n"
            annotfile.write(lines)
    
    def writeFastg(self, sequences, file, mode='w'):
        lines = ""
        with open(file,mode) as graphfile:
            for seq in sequences:
                lines += f"{seq}"
            graphfile.write(lines)
    """
    def pathToNeighbor(path):
        nbrlist = []
        if type(edges) == list:
            for edge in edges:
                edge = edge[0]
                if edge in self.edges.keys():
                    neighbor = self.edges[edge]
                    
                    if neighbor != None:
                        nbrlist += neighbor
                    
                else: pass
        else:
            if edges in self.edges.keys():
                
                neighbor = self.edges[edges]
               
                if neighbor != None:
                    nbrlist += neighbor
                    
            else:
                pass
    """
    def filterGraph(self, fastgfile, contigs, neighbors=0):
        """
        Goal of the function is to output a fastg file that can be loaded into Bandage
        with just a subset of the graph. The function first looks up all contigs
        belonging to a structure, next it looks up what nodes make up this contig.
        Next, it optionally looks up if there are more neighbors to these contigs.
        ## TODO:
        Next it will look up all neighbors in the graph file for their sequence headers 
        and sequences, and finally it will reconstruct the graph. 
        
        This function could work well as a workflow in a Jupyter notebook.
        """
        # Look up all the contigs

        if type(contigs) == str:
            contigs = [contigs]
        #structure_contigs = ["NODE_82_length_109641_cov_51.240377"]
        # Crossreference contigs with paths                
        paths = [self.contigLookup(x,"path") for x in contigs]
        logging.debug(f"PATHS\n{paths}")
        self.writeAnnotation(paths, "contig","annotfile.csv")
        # Lookup neighbors in the graph (user flexibility not yet implemented)
        nbrdict = {}
        for i in paths:
            for j in i:
                nbrs = self.expandSubgraph(j, neighbors=1)
                nbrdict[j] = nbrs

        coll_nbrdict = defaultdict(list)
        for key,value in nbrdict.items():
            coll_nbrdict[key] += value[0]
            for v in value[0]:
                coll_nbrdict[v].append(key)

        sequences = []
        for node in coll_nbrdict.keys(): 
            node_nbrs= coll_nbrdict[node]
            keynode_fastg = self.reconstructOneFastg(node,node_nbrs)
            sequences.append(keynode_fastg)
        
        self.writeFastg(sequences, fastgfile)



    def isContigSubgraph(self, contig):
        path = self.contigLookup(contig, 'path')
        nbrs = self.expandSubgraph(path,neighbors=1)
        path = [x[:-1] for x in path]
        nbrkeys = [x[:-1] for x in nbrs.keys()]
        logging.debug(f"COMPARING {contig}: {set(nbrkeys)} and {set(path)}.\n diff: {len(set(nbrkeys))}:{len(set(path))}")
        return(set(nbrkeys) == set(path))

        




if __name__ == "__main__":
    print("Testing the tool")
    #constructGraphConnection("test_metaspades/assembly_graph.fastg","test_metaspades/contigs.paths","testyaml.yml",4,5)
    #constructGraphConnection("test_metaspades/assembly_graph.fastg","test_metaspades/contigs.paths","full_testyaml.out",4,5)
    #GCObject = GraphConnection("testdata/test_metaspades/assembly_graph.fastg","testdata/test_metaspades/contigs.paths","testdata/small_testyaml.out")
    mmg_obj = GraphConnection(graphfile = "testdata/test_graph.fastg",pathsfile = "testdata/test_contigs.paths",annotation = "testdata/test_MetaMobilePicker.out")
    paths = mmg_obj.contigLookup("NODE_508_length_9760_cov_42.639464","path")
    plsm = mmg_obj.lookupMGEs("plasmid")
    print(plsm)

    testsub = 'NODE_345_length_24440_cov_21.508304'
    issub = mmg_obj.isContigSubgraph(testsub)
    print(issub)
    #print(GCObject.nodeLookup("2778827"))
    #GCObject.graphSequences("testdata/test_metaspades/assembly_graph.fastg")
    #GCObject.filterGraph(fastgfile="testplasmid.fastg",contigs="NODE_82_length_109641_cov_51.240377")

